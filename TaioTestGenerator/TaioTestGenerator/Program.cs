﻿using CommandLine;
using System;
using System.Diagnostics;

namespace TaioTestGenerator
{
    class Options
    {
        [Option('m', Required = false, HelpText = "Path to Monika's TAIO program")]
        public string PathToMonikasProgram { get; set; }

        [Option('t', Required = true, HelpText = "Path to Tomek's TAIO program")]
        public string PathToTomeksProgram { get; set; }


        [Option('n', Required = false, Default = 0, HelpText = "Program is running until 'n' tests run or difference between programs is found. " +
                                                               "N=0 means that program is running until difference is found")]
        public int NumberOfTests { get; set; }

        [Option(longName: "maxpieces", Required = false, Default = 5)]
        public int MaxNumberOfPieces { get; set; }

        [Option(longName: "minpieces", Required = false, Default = 1)]
        public int MinNumberOfPieces { get; set; }

        [Option(longName: "maxsize", Required = false, Default = 15)]
        public int MaxSize { get; set; }

        [Option(longName: "minsize", Required = false, Default = 5)]
        public int MinSize { get; set; }

        [Option(longName: "maxpiecesize", Required = false, Default = 15)]
        public int MaxPieceSize { get; set; }

        [Option(longName: "minpiecesize", Required = false, Default = 5)]
        public int MinPieceSize { get; set; }

        [Option(longName: "minvalue", Required = false, Default = 1)]
        public int MinValue { get; set; }

        [Option(longName: "maxvalue", Required = false, Default = 10)]
        public int MaxValue { get; set; }


    }

    class Program
    {
        static void Main(string[] args) => Parser.Default.ParseArguments<Options>(args)
                                           .MapResult(
                                           options => RunAndReturnExitCode(options),
                                           _ => 1);

        private static int RunAndReturnExitCode(Options options)
        {
            try
            {
                var testGenerator = new TestGenerator(options);
                var stopwatch = new Stopwatch();

                for (int i = 0; i < options.NumberOfTests || options.NumberOfTests == 0; i++)
                {
                    var test = testGenerator.GenerateTest();

                    Console.WriteLine("Generated test: ");
                    Console.WriteLine(test);

                    stopwatch.Reset();

                    stopwatch.Start();
                    var resultTomek = RunProgramWithTest(options.PathToTomeksProgram, test).Split()[4].Trim();
                    stopwatch.Stop();
                    Console.Write("Tomek: " + stopwatch.Elapsed.ToString(@"mm\:ss\:ff") + " " + resultTomek + Environment.NewLine);

                    if(!string.IsNullOrWhiteSpace(options.PathToMonikasProgram))
                    {
                        stopwatch.Start();
                        var resultMonika = RunLibraryWithTest(options.PathToMonikasProgram, test).Split()[0].Trim(); // Monika program is DLL file, need to handle differently
                        stopwatch.Stop();
                        Console.Write("Monika: " + stopwatch.Elapsed.ToString(@"mm\:ss\:ff") + " " + resultMonika + ", ");
                        stopwatch.Reset();
                        if (resultMonika != resultTomek)
                        {
                            Console.WriteLine("Different output !!!oneone111!1!");
                            Console.WriteLine("Test: ");
                            Console.WriteLine(test);
                            break;
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return 1;
            }
            return 0;
        }

        private static string RunProgramWithTest(string programPath, string test)
        {
            var processStartInfo = new ProcessStartInfo()
            {
                UseShellExecute = false,
                RedirectStandardInput = true,
                RedirectStandardOutput = true,
                RedirectStandardError = true,
                FileName = programPath,
            };

            var process = new Process()
            {
                StartInfo = processStartInfo
            };
            process.Start();

            process.StandardInput.WriteLine(test);
            process.WaitForExit();
            var outputString = process.StandardOutput.ReadToEnd();
            process.Close();

            return outputString;
        }

        private static string RunLibraryWithTest(string programPath, string test)
        {
            var processStartInfo = new ProcessStartInfo()
            {
                UseShellExecute = false,
                RedirectStandardInput = true,
                RedirectStandardOutput = true,
                RedirectStandardError = true,
                FileName = "dotnet",
                Arguments = programPath
            };

            var process = new Process()
            {
                StartInfo = processStartInfo
            };
            process.Start();

            process.StandardInput.WriteLine(test);
            process.WaitForExit();
            var outputString = process.StandardOutput.ReadToEnd();
            process.Close();

            return outputString;
        }

    }
}
