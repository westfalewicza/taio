﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaioTestGenerator
{
    internal class TestGenerator
    {
        private Options options;

        public TestGenerator(Options options)
        {
            this.options = options;
        }

        internal string GenerateTest()
        {
            Random rnd = new Random();

            var boardWidth = rnd.Next(options.MinSize, options.MaxSize);
            var boardHeight = rnd.Next(options.MinSize, options.MaxSize);

            var numberOfPieces = rnd.Next(options.MinNumberOfPieces, options.MaxNumberOfPieces);
            List<Piece> pieces = new List<Piece>();

            for(int i=0; i<numberOfPieces; i++)
            {
                var width = rnd.Next(Math.Min(options.MinPieceSize, boardWidth), Math.Min(options.MaxPieceSize, boardWidth));
                var height = rnd.Next(Math.Min(options.MinPieceSize, boardWidth), Math.Min(options.MaxPieceSize, boardHeight));
                var value = rnd.Next(options.MinValue, options.MaxValue);

                pieces.Add(new Piece()
                {
                    Width = width,
                    Height = height,
                    Value = value
                });
            }

            StringBuilder testStringBuilder = new StringBuilder();

            testStringBuilder.AppendLine($"{boardWidth} {boardHeight}");
            testStringBuilder.AppendLine(numberOfPieces.ToString());
            foreach(var piece in pieces)
            {
                testStringBuilder.AppendLine($"{piece.Width} {piece.Height} {piece.Value}");
            }

            return testStringBuilder.ToString();
        }
    }

    internal class Piece
    {
        public int Width { get; set; }

        public int Height { get; set; }

        public int Value { get; set; }
    }
}