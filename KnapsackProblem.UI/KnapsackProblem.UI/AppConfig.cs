﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KnapsackProblem.UI
{
    public static class AppConfig
    {
        static public string SolverPath { get; set; } = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "solver.exe");
    }
}
