﻿using KnapsackProblem.UI.Models;
using KnapsackProblem.UI.Utilities;
using System.Diagnostics;

namespace KnapsackProblem.UI.Services
{
    public class KnapsackSolverRunner
    {
        public OutputInfo RunSolver(InputInfo input)
        {
            var processStartInfo = new ProcessStartInfo()
            {
                UseShellExecute = false,
                RedirectStandardInput = true,
                RedirectStandardOutput = true,
                RedirectStandardError = true,
                FileName = AppConfig.SolverPath
            };

            var process = new Process()
            {
                StartInfo = processStartInfo
            };
            process.Start();

            var inputString = KnapsackSolverDataParser.ParseInputInfoToString(input);
            process.StandardInput.WriteLine(inputString);
            process.WaitForExit();
            var outputString = process.StandardOutput.ReadToEnd();
            process.Close();

            return KnapsackSolverDataParser.ParseStringOutputToOutputInfo(outputString);
        }
    }
}
