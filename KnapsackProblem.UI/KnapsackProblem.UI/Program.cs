﻿using System;
using System.IO;
using System.Windows.Forms;

namespace KnapsackProblem.UI
{
    static class Program
    {
        /// <summary>
        /// Główny punkt wejścia dla aplikacji.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            if (args.Length > 0)
            {
                AppConfig.SolverPath = args[0];
            }

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            if (!File.Exists(AppConfig.SolverPath))
            {
                MessageBox.Show($"Solver file: \"{AppConfig.SolverPath}\" doesn't exists!", "Incorrect solver path", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                Application.Run(new Form1());
            }
        }
    }
}
