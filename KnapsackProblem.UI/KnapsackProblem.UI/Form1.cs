﻿using KnapsackProblem.UI.Models;
using KnapsackProblem.UI.Services;
using KnapsackProblem.UI.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KnapsackProblem.UI
{
    public partial class Form1 : Form
    {
        private List<Element> Elements = new List<Element>();
        private Random randomColorGenerator = new Random(44);
        private Knapsack CurrentKnapsack;
        private int pixelsPerUnit;

        private Element currentElement;


        public Form1()
        {
            InitializeComponent();
            InitializeWindowSize();

        }



        public void InitializeWindowSize()
        {
            this.MaximumSize = Screen.PrimaryScreen.Bounds.Size;
            this.MinimumSize = Screen.PrimaryScreen.Bounds.Size;
        }

        public void DeleteCurrentElement()
        {
            if (currentElement == null)
                return;

            Elements.Remove(currentElement);
            ReorganizeID();
            RemoveSolution();
            UpdateElements();
            currentElement = null;
            RefreshLabels();
            return;
        }

        private void RefreshLabels()
        {
            EditElementWidthTextBox.Text = "";
            EditElementHeightTextBox.Text = "";
            EditElementValueTextBox.Text = "";
            InfoWidthValueLabel.Text = "";
            InfoHeightValueLabel.Text = "";
            InfoValueValueLabel.Text = "";
            EditElementWidthTextBox.Enabled = false;
            EditElementHeightTextBox.Enabled = false;
            EditElementValueTextBox.Enabled = false;
            EditElementSaveButton.Enabled = false;
        }

        public void ReorganizeID()
        {
            for (int i = 0; i < Elements.Count; i++)
            {
                Elements[i].ID = i;
            }
        }

        public void UpdateElements()
        {
            AllElementsFlowLayoutPanel.Controls.Clear();
            foreach (var el in Elements)
            {
                el.PixelPerUnit = pixelsPerUnit;
                AddElementToFlowLayoutPanel(el);
            }
        }

        private void AddElementToFlowLayoutPanel(Element el)
        {
            ElementPictureBox elementPictureBox = new ElementPictureBox(el, pixelsPerUnit);
            elementPictureBox.MouseDown += ElementClick;
            AllElementsFlowLayoutPanel.Controls.Add(elementPictureBox);

        }

        private void wczytajTestToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Element.elementCount = 0;
            OpenFileDialog opDialog = new OpenFileDialog();
            opDialog.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            if (opDialog.ShowDialog() == DialogResult.OK)
            {
                FileStream fileStream = opDialog.OpenFile() as FileStream;
                StreamReader sr = new StreamReader(fileStream);
                string KnapsackSize = sr.ReadLine();
                KnapsackSize = TrimStartingSpaces(KnapsackSize);
                string[] splites = KnapsackSize.Split(' ');

                bool result1 = int.TryParse(KnapsackSize.Split(' ')[0], out int KnapsackWidth);

                bool result2 = int.TryParse(KnapsackSize.Split(' ')[1], out int KnapsackHeight);

                if (!result1 || !result2)
                {
                    MessageBox.Show("Błędnie utworzony plik testowy", "Błąd wczytywania pliku");
                    return;
                }

                CountPixelsPerUnit(KnapsackWidth, KnapsackHeight, KnapsackPictureBox.Width, KnapsackPictureBox.Height);

                bool result3 = int.TryParse(sr.ReadLine(), out int elementsCount);

                if (!result3)
                {
                    MessageBox.Show("Błędnie utworzony plik testowy", "Błąd wczytywania pliku");
                    return;
                }

                List<int> widths = new List<int>();
                List<int> heights = new List<int>();
                List<int> values = new List<int>();

                for (int i = 0; i < elementsCount; i++)
                {
                    string element = sr.ReadLine();
                    if (String.IsNullOrEmpty(element))
                    {
                        MessageBox.Show("Błędnie utworzony plik testowy. Najprawdopodobniej podana zbyt duża liczba elementów", "Błąd wczytywania pliku");
                        return;
                    }
                    string[] characteristics = element.Split(' ');
                    List<string> characteristicsList = new List<string>();
                    for (int j = 0; j < characteristics.Length; j++)
                    {
                        if (characteristics[j].Length == 0)
                            continue;
                        characteristicsList.Add(characteristics[j]);
                    }
                    bool resultW = int.TryParse(characteristicsList[0], out int width);
                    bool resultH = int.TryParse(characteristicsList[1], out int height);
                    bool resultV = int.TryParse(characteristicsList[2], out int value);

                    if (!resultW || !resultH || !resultV)
                    {
                        MessageBox.Show("Błędnie utworzony plik testowy", "Błąd wczytywania pliku");
                        return;
                    }

                    widths.Add(width);
                    heights.Add(height);
                    values.Add(value);
                }

                DeleteAllElementsToolStripMenuItem_Click(null, null);

                KnapsackWidthTextBox.Text = KnapsackWidth.ToString();
                KnapsackHeightTextBox.Text = KnapsackHeight.ToString();
                SetKnapsackSizeButton_Click(null, null);

                CountTextBox.Text = "1";
                for (int i = 0; i < widths.Count; i++)
                {
                    Color color = Color.FromArgb(randomColorGenerator.Next(0, 256), randomColorGenerator.Next(0, 256), randomColorGenerator.Next(0, 256));
                    Element currentElement = new Element(widths[i], heights[i], values[i], color, pixelsPerUnit);
                    Elements.Add(currentElement);

                    RescaleIfTooBig(currentElement.Width, currentElement.Height);
                }

                UpdateElements();
                UpdateKnapsack();

                WidthTextBox.Text = "";
                HeightTextBox.Text = "";
                ValueTextBox.Text = "";
                CountTextBox.Text = "";

                fileStream.Close();
            }
        }

        public string TrimStartingSpaces(string str)
        {
            for (int i = 0; i < str.Length; i++)
            {
                if (str[i] == ' ')
                    continue;
                else
                    return str.Substring(i);
            }
            return "";
        }

        private void AddElementButton_Click(object sender, EventArgs e)
        {

            if (CurrentKnapsack == null)
            {
                MessageBox.Show("Najpierw utwórz nowy plecak!", "Zła kolejność wprowadzania danych!");
                return;
            }

            bool result1 = int.TryParse(WidthTextBox.Text, out int width);
            bool result2 = int.TryParse(HeightTextBox.Text, out int height);
            bool result3 = int.TryParse(ValueTextBox.Text, out int value);
            bool result4 = int.TryParse(CountTextBox.Text, out int count);

            bool result5 = string.IsNullOrEmpty(WidthTextBox.Text);
            bool result6 = string.IsNullOrEmpty(HeightTextBox.Text);
            bool result7 = string.IsNullOrEmpty(ValueTextBox.Text);
            bool result8 = string.IsNullOrEmpty(CountTextBox.Text);

            if (!result1 || !result2 || !result3 || !result4 || result5 || result6 || result7 || result8)
            {
                MessageBox.Show("Proszę podać prawidłowe argumenty - liczby całkowite", "Błąd wprowadzania danych");
                return;
            }

            if (Elements.Count > 0)
            {
                Element.elementCount = Elements.Last().ID + 1;
            }
            else
            {
                Element.elementCount = 0;
            }

            for (int i = 0; i < count; i++)
            {
                Color color = Color.FromArgb(randomColorGenerator.Next(0, 256), randomColorGenerator.Next(0, 256), randomColorGenerator.Next(0, 256));
                Element currentElement = new Element(width, height, value, color, pixelsPerUnit);
                Elements.Add(currentElement);
            }

            RescaleIfTooBig(width, height);
            RemoveSolution();
            UpdateElements();
        }

        private void RescaleIfTooBig(int width, int height)
        {
            if (width * pixelsPerUnit > AllElementsFlowLayoutPanel.Size.Width - Config.WidthTreshold)
            {
                pixelsPerUnit = (AllElementsFlowLayoutPanel.Size.Width - Config.WidthTreshold + 20) / width;
            }
            if (height * pixelsPerUnit > AllElementsFlowLayoutPanel.Size.Height - Config.HeightTreshold)
            {
                pixelsPerUnit = (AllElementsFlowLayoutPanel.Size.Height - Config.HeightTreshold + 20) / height;
            }

            UpdateElements();
            UpdateKnapsack();
        }

        private void UpdateKnapsack()
        {
            this.CurrentKnapsack.PixelsPerUnit = pixelsPerUnit;
            CurrentKnapsack.DrawGrid();
            KnapsackPictureBox.Refresh();
        }

        private void ElementClick(object sender, EventArgs e)
        {
            ElementPictureBox elementPictureBox = sender as ElementPictureBox;
            Element element = elementPictureBox.element;
            foreach (ElementPictureBox el in AllElementsFlowLayoutPanel.Controls)
            {
                el.Image = el.element.View.Clone() as Image;
            }
            elementPictureBox.DrawSelect();
            this.InfoWidthValueLabel.Text = element.Width.ToString();
            this.InfoHeightValueLabel.Text = element.Height.ToString();
            this.InfoValueValueLabel.Text = element.Value.ToString();
            currentElement = elementPictureBox.element;
            EditElementHeightTextBox.Enabled = true;
            EditElementWidthTextBox.Enabled = true;
            EditElementValueTextBox.Enabled = true;
            EditElementSaveButton.Enabled = true;
            EditElementHeightTextBox.Text = element.Height.ToString();
            EditElementWidthTextBox.Text = element.Width.ToString();
            EditElementValueTextBox.Text = element.Value.ToString();
        }

        private void DeleteAllElementsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Elements.Clear();
            AllElementsFlowLayoutPanel.Controls.Clear();
            AllElementsFlowLayoutPanel.Refresh();
        }

        private void SetKnapsackSizeButton_Click(object sender, EventArgs e)
        {
            bool result1 = int.TryParse(KnapsackWidthTextBox.Text, out int width);
            bool result2 = int.TryParse(KnapsackHeightTextBox.Text, out int height);

            bool result3 = string.IsNullOrEmpty(KnapsackWidthTextBox.Text);
            bool result4 = string.IsNullOrEmpty(KnapsackHeightTextBox.Text);

            if (!result1 || !result2 || result3 || result4)
            {
                MessageBox.Show("Proszę podać prawidłowe argumenty - liczby całkowite", "Błąd wprowadzania danych");
                return;
            }

            this.KnapsackSizeValueLabel.Text = "(" + width.ToString() + ", " + height.ToString() + ")";
            this.KnapsackSpaceValueLabel.Text = (width * height).ToString();
            this.UsedSpaceValueLabel.Text = "0";
            this.KnapsackValueValueLabel.Text = "0";
            this.KnapsackElementsCountValueLabel.Text = "0";

            int KnapsackPictureWidth = KnapsackPictureBox.Size.Width;
            int KnapsackPictureHeight = KnapsackPictureBox.Size.Height;

            if (CurrentKnapsack != null)
                RemoveSolution();

            pixelsPerUnit = CountPixelsPerUnit(width, height, KnapsackPictureWidth, KnapsackPictureHeight);

            CurrentKnapsack = new Knapsack(width, height, KnapsackPictureBox.Size, Elements, pixelsPerUnit);
            CurrentKnapsack.DrawGrid();
            this.KnapsackPictureBox.Image = CurrentKnapsack.Picture;
            foreach (var el in Elements)
                RescaleIfTooBig(el.Width, el.Height);
        }

        private int CountPixelsPerUnit(int width, int height, int KnapsackPictureWidth, int KnapsackPictureHeight)
        {
            int pixels1 = KnapsackPictureWidth / width;
            int pixels2 = KnapsackPictureHeight / height;

            return Math.Min(pixels1, pixels2);
        }

        private void StartTest_Click(object sender, EventArgs e)
        {
            RemoveSolution();
            

            List<ElementInfo> elementsInfo = new List<ElementInfo>();
            foreach (var el in Elements)
            {
                elementsInfo.Add(new ElementInfo()
                {
                    Width = el.Width,
                    Height = el.Height,
                    Value = el.Value
                });
            }

            InputInfo inputInfo = new InputInfo()
            {
                BoardWidth = CurrentKnapsack.Width,
                BoardHeight = CurrentKnapsack.Height,
                Elements = elementsInfo
            };

            KnapsackSolverRunner knapsackSolverRunner = new KnapsackSolverRunner();
            OutputInfo outputInfo = knapsackSolverRunner.RunSolver(inputInfo);

            List<Element> outputElements = new List<Element>();
            List<Point> outputPoints = new List<Point>();
            int areaSum = 0;
            foreach (var el in outputInfo.Elements)
            {
                Element LiveElement = Elements.Where(x => x.ID == el.Id).ToList()[0];
                Color currentColor = LiveElement.Color;
                LiveElement.IsInKnapsack = true;
                LiveElement.Refresh();
                Element currentElement = new Element(el.Width, el.Height, el.Value, currentColor, pixelsPerUnit);
                outputElements.Add(currentElement);
                outputPoints.Add(el.Position);
                areaSum += el.Width * el.Height;
            }
            foreach (ElementPictureBox el in AllElementsFlowLayoutPanel.Controls)
            {
                el.Image = el.element.View.Clone() as Image;
            }
            KnapsackElementsCountValueLabel.Text = outputElements.Count.ToString();
            UsedSpaceValueLabel.Text = areaSum.ToString();
            KnapsackValueValueLabel.Text = outputInfo.KnapsackValue.ToString();

            UpdateElements();
            CurrentKnapsack.GeneratePicture(outputElements, outputPoints);
            KnapsackPictureBox.Refresh();



        }

        private void RemoveSolution()
        {
            foreach (var el in Elements)
                el.IsInKnapsack = false;
            Graphics.FromImage(this.KnapsackPictureBox.Image).Clear(Color.White);
            CurrentKnapsack.DrawGrid();
            this.KnapsackPictureBox.Image = CurrentKnapsack.Picture;
            KnapsackPictureBox.Refresh();
        }

        private void zapiszToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.FileName = "Test.txt";
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                if (CurrentKnapsack == null)
                {
                    MessageBox.Show("Nie ma czego zapisywać", "Błąd zapisu do pliku");
                    return;
                }


                FileStream fs = saveFileDialog.OpenFile() as FileStream;
                StreamWriter sw = new StreamWriter(fs);

                string KnapsackSize = CurrentKnapsack.Width.ToString() + " " + CurrentKnapsack.Height.ToString();
                string ElementCount = CurrentKnapsack.allElementsAvailable.Count.ToString();

                sw.WriteLine(KnapsackSize);
                sw.WriteLine(ElementCount);

                foreach (var el in CurrentKnapsack.allElementsAvailable)
                {
                    string line = el.Width.ToString() + " " + el.Height.ToString() + " " + el.Value.ToString();
                    sw.WriteLine(line);
                }
                sw.Flush();
                fs.Close();
            }
        }

        private void keyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                DeleteCurrentElement();
            }
            if (e.KeyCode == Keys.Up && e.Modifiers == Keys.Control)
            {
                if (CurrentKnapsack != null)
                    Rescale(Config.RescaleFactor);
            }
            if (e.KeyCode == Keys.Down && e.Modifiers == Keys.Control)
            {
                if (CurrentKnapsack != null)
                    Rescale(-Config.RescaleFactor);
            }
            if (e.KeyCode == Keys.Right && e.Modifiers ==Keys.Control)
            {
                if (CurrentKnapsack != null)
                {
                    int maxPixelsPerUnit = CountPixelsPerUnit(CurrentKnapsack.Width, CurrentKnapsack.Height, KnapsackPictureBox.Width, KnapsackPictureBox.Height);
                    pixelsPerUnit = maxPixelsPerUnit;
                    RemoveSolution();
                    UpdateKnapsack();
                    UpdateElements();

                }
            }



        }

        private void Rescale(int rescaleFactor)
        {
            int maxPixelsPerUnit = CountPixelsPerUnit(CurrentKnapsack.Width, CurrentKnapsack.Height, KnapsackPictureBox.Width, KnapsackPictureBox.Height);
            if (pixelsPerUnit + rescaleFactor > 0 && pixelsPerUnit + rescaleFactor <= maxPixelsPerUnit)
                pixelsPerUnit += rescaleFactor;
            UpdateKnapsack();
            RemoveSolution();
            UpdateElements();
        }

        private void EditElementSaveButton_Click(object sender, EventArgs e)
        {
            bool result1 = int.TryParse(EditElementWidthTextBox.Text, out int width);
            bool result2 = int.TryParse(EditElementHeightTextBox.Text, out int height);
            bool result3 = int.TryParse(EditElementValueTextBox.Text, out int value);

            bool result4 = string.IsNullOrEmpty(EditElementWidthTextBox.Text);
            bool result5 = string.IsNullOrEmpty(EditElementHeightTextBox.Text);
            bool result6 = string.IsNullOrEmpty(EditElementValueTextBox.Text);

            if (!result1 || !result2 || !result3 || result4 || result5 || result6)
            {
                MessageBox.Show("Proszę podać prawidłowe argumenty - liczby całkowite", "Błąd wprowadzania danych");
                return;
            }

            currentElement.Width = width;
            currentElement.Height = height;
            currentElement.Value = value;

            InfoWidthValueLabel.Text = width.ToString();
            InfoHeightValueLabel.Text = height.ToString();
            InfoValueValueLabel.Text = value.ToString();

            RemoveSolution();
            RescaleIfTooBig(width, height);

            foreach (ElementPictureBox el in AllElementsFlowLayoutPanel.Controls)
                if (el.element.ID == currentElement.ID)
                {
                    el.DrawSelect();
                }


        }
    }
}
