﻿namespace KnapsackProblem.UI
{
    partial class Form1
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Wymagana metoda obsługi projektanta — nie należy modyfikować 
        /// zawartość tej metody z edytorem kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.rootLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.plikToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.wczytajToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.zapiszToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.usuńWszystkieKlockiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tablePanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.ElementsTablePanel = new System.Windows.Forms.TableLayoutPanel();
            this.AddElementTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.AddElementsLabel = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.CountTextBox = new System.Windows.Forms.TextBox();
            this.ValueTextBox = new System.Windows.Forms.TextBox();
            this.HeightTextBox = new System.Windows.Forms.TextBox();
            this.WidthLabel = new System.Windows.Forms.Label();
            this.CountLabel = new System.Windows.Forms.Label();
            this.ValueLabel = new System.Windows.Forms.Label();
            this.HeightLabel = new System.Windows.Forms.Label();
            this.WidthTextBox = new System.Windows.Forms.TextBox();
            this.AddElementButton = new System.Windows.Forms.Button();
            this.SetKnapsackSizeTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.KnapsackHeightTextBox = new System.Windows.Forms.TextBox();
            this.KnapsackWidthLabel = new System.Windows.Forms.Label();
            this.KnapsackHeightLabel = new System.Windows.Forms.Label();
            this.KnapsackWidthTextBox = new System.Windows.Forms.TextBox();
            this.SetKnapsackSizeLabel = new System.Windows.Forms.Label();
            this.SetKnapsackSizeButton = new System.Windows.Forms.Button();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.EditElementSaveButton = new System.Windows.Forms.Button();
            this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
            this.EditElementValueTextBox = new System.Windows.Forms.TextBox();
            this.EditElementValueLabel = new System.Windows.Forms.Label();
            this.EditElementHeightTextBox = new System.Windows.Forms.TextBox();
            this.EditElementWidthLabel = new System.Windows.Forms.Label();
            this.EditElementHeightLabel = new System.Windows.Forms.Label();
            this.EditElementWidthTextBox = new System.Windows.Forms.TextBox();
            this.EditElementLabel = new System.Windows.Forms.Label();
            this.ScoreTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.KnapsackInfoLabel = new System.Windows.Forms.Label();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.KnapsackSizeValueLabel = new System.Windows.Forms.Label();
            this.KnapsackElementsCountValueLabel = new System.Windows.Forms.Label();
            this.KnapsackValueValueLabel = new System.Windows.Forms.Label();
            this.UsedSpaceValueLabel = new System.Windows.Forms.Label();
            this.KnapsackSpaceLabel = new System.Windows.Forms.Label();
            this.KnapsackElementsCountLabel = new System.Windows.Forms.Label();
            this.KnapsackValueLabel = new System.Windows.Forms.Label();
            this.UsedSpaceLabel = new System.Windows.Forms.Label();
            this.KnapsackSpaceValueLabel = new System.Windows.Forms.Label();
            this.KnapsackSizeLabel = new System.Windows.Forms.Label();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.ElementInfoLabel = new System.Windows.Forms.Label();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.InfoValueValueLabel = new System.Windows.Forms.Label();
            this.InfoHeightValueLabel = new System.Windows.Forms.Label();
            this.InfoWidthLabel = new System.Windows.Forms.Label();
            this.InfoValueLabel = new System.Windows.Forms.Label();
            this.InfoHeightLabel = new System.Windows.Forms.Label();
            this.InfoWidthValueLabel = new System.Windows.Forms.Label();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.AllElementsFlowLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.KnapsackPictureBox = new System.Windows.Forms.PictureBox();
            this.StartTest = new System.Windows.Forms.Button();
            this.plikToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.wczytajTestToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.zapiszToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.DeleteAllElementsStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rootLayoutPanel.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.tablePanel2.SuspendLayout();
            this.ElementsTablePanel.SuspendLayout();
            this.AddElementTableLayoutPanel.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SetKnapsackSizeTableLayoutPanel.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            this.tableLayoutPanel9.SuspendLayout();
            this.ScoreTableLayoutPanel.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.KnapsackPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // rootLayoutPanel
            // 
            this.rootLayoutPanel.ColumnCount = 1;
            this.rootLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.rootLayoutPanel.Controls.Add(this.menuStrip1, 0, 0);
            this.rootLayoutPanel.Controls.Add(this.tablePanel2, 0, 1);
            this.rootLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rootLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.rootLayoutPanel.Margin = new System.Windows.Forms.Padding(4);
            this.rootLayoutPanel.Name = "rootLayoutPanel";
            this.rootLayoutPanel.RowCount = 2;
            this.rootLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.rootLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.rootLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.rootLayoutPanel.Size = new System.Drawing.Size(1180, 624);
            this.rootLayoutPanel.TabIndex = 0;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.plikToolStripMenuItem1,
            this.usuńWszystkieKlockiToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1180, 24);
            this.menuStrip1.TabIndex = 0;
            // 
            // plikToolStripMenuItem1
            // 
            this.plikToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.wczytajToolStripMenuItem,
            this.zapiszToolStripMenuItem1});
            this.plikToolStripMenuItem1.Name = "plikToolStripMenuItem1";
            this.plikToolStripMenuItem1.Size = new System.Drawing.Size(38, 20);
            this.plikToolStripMenuItem1.Text = "Plik";
            // 
            // wczytajToolStripMenuItem
            // 
            this.wczytajToolStripMenuItem.Name = "wczytajToolStripMenuItem";
            this.wczytajToolStripMenuItem.Size = new System.Drawing.Size(115, 22);
            this.wczytajToolStripMenuItem.Text = "Wczytaj";
            this.wczytajToolStripMenuItem.Click += new System.EventHandler(this.wczytajTestToolStripMenuItem_Click);
            // 
            // zapiszToolStripMenuItem1
            // 
            this.zapiszToolStripMenuItem1.Name = "zapiszToolStripMenuItem1";
            this.zapiszToolStripMenuItem1.Size = new System.Drawing.Size(115, 22);
            this.zapiszToolStripMenuItem1.Text = "Zapisz";
            this.zapiszToolStripMenuItem1.Click += new System.EventHandler(this.zapiszToolStripMenuItem_Click);
            // 
            // usuńWszystkieKlockiToolStripMenuItem
            // 
            this.usuńWszystkieKlockiToolStripMenuItem.Name = "usuńWszystkieKlockiToolStripMenuItem";
            this.usuńWszystkieKlockiToolStripMenuItem.Size = new System.Drawing.Size(132, 20);
            this.usuńWszystkieKlockiToolStripMenuItem.Text = "Usuń wszystkie klocki";
            this.usuńWszystkieKlockiToolStripMenuItem.Click += new System.EventHandler(this.DeleteAllElementsToolStripMenuItem_Click);
            // 
            // tablePanel2
            // 
            this.tablePanel2.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tablePanel2.ColumnCount = 3;
            this.tablePanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tablePanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.tablePanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tablePanel2.Controls.Add(this.ElementsTablePanel, 0, 0);
            this.tablePanel2.Controls.Add(this.ScoreTableLayoutPanel, 2, 0);
            this.tablePanel2.Controls.Add(this.tableLayoutPanel7, 1, 0);
            this.tablePanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tablePanel2.Location = new System.Drawing.Point(0, 26);
            this.tablePanel2.Margin = new System.Windows.Forms.Padding(0);
            this.tablePanel2.Name = "tablePanel2";
            this.tablePanel2.RowCount = 1;
            this.tablePanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tablePanel2.Size = new System.Drawing.Size(1180, 598);
            this.tablePanel2.TabIndex = 1;
            // 
            // ElementsTablePanel
            // 
            this.ElementsTablePanel.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.ElementsTablePanel.ColumnCount = 1;
            this.ElementsTablePanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.ElementsTablePanel.Controls.Add(this.AddElementTableLayoutPanel, 0, 2);
            this.ElementsTablePanel.Controls.Add(this.SetKnapsackSizeTableLayoutPanel, 0, 1);
            this.ElementsTablePanel.Controls.Add(this.tableLayoutPanel8, 0, 0);
            this.ElementsTablePanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ElementsTablePanel.Location = new System.Drawing.Point(1, 1);
            this.ElementsTablePanel.Margin = new System.Windows.Forms.Padding(0);
            this.ElementsTablePanel.Name = "ElementsTablePanel";
            this.ElementsTablePanel.RowCount = 3;
            this.ElementsTablePanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.ElementsTablePanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.ElementsTablePanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.ElementsTablePanel.Size = new System.Drawing.Size(176, 596);
            this.ElementsTablePanel.TabIndex = 0;
            // 
            // AddElementTableLayoutPanel
            // 
            this.AddElementTableLayoutPanel.ColumnCount = 1;
            this.AddElementTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.AddElementTableLayoutPanel.Controls.Add(this.AddElementsLabel, 0, 0);
            this.AddElementTableLayoutPanel.Controls.Add(this.tableLayoutPanel1, 0, 1);
            this.AddElementTableLayoutPanel.Controls.Add(this.AddElementButton, 0, 2);
            this.AddElementTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.AddElementTableLayoutPanel.Location = new System.Drawing.Point(1, 358);
            this.AddElementTableLayoutPanel.Margin = new System.Windows.Forms.Padding(0);
            this.AddElementTableLayoutPanel.Name = "AddElementTableLayoutPanel";
            this.AddElementTableLayoutPanel.RowCount = 3;
            this.AddElementTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.AddElementTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.AddElementTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.AddElementTableLayoutPanel.Size = new System.Drawing.Size(174, 237);
            this.AddElementTableLayoutPanel.TabIndex = 3;
            // 
            // AddElementsLabel
            // 
            this.AddElementsLabel.AutoSize = true;
            this.AddElementsLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.AddElementsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AddElementsLabel.Location = new System.Drawing.Point(4, 0);
            this.AddElementsLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.AddElementsLabel.Name = "AddElementsLabel";
            this.AddElementsLabel.Size = new System.Drawing.Size(166, 50);
            this.AddElementsLabel.TabIndex = 2;
            this.AddElementsLabel.Text = "Dodaj nowy kawałek";
            this.AddElementsLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.CountTextBox, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.ValueTextBox, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.HeightTextBox, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.WidthLabel, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.CountLabel, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.ValueLabel, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.HeightLabel, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.WidthTextBox, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(4, 53);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(166, 152);
            this.tableLayoutPanel1.TabIndex = 3;
            // 
            // CountTextBox
            // 
            this.CountTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.CountTextBox.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.CountTextBox.Location = new System.Drawing.Point(83, 118);
            this.CountTextBox.Margin = new System.Windows.Forms.Padding(0);
            this.CountTextBox.Name = "CountTextBox";
            this.CountTextBox.Size = new System.Drawing.Size(82, 27);
            this.CountTextBox.TabIndex = 7;
            // 
            // ValueTextBox
            // 
            this.ValueTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.ValueTextBox.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.ValueTextBox.Location = new System.Drawing.Point(83, 79);
            this.ValueTextBox.Margin = new System.Windows.Forms.Padding(0);
            this.ValueTextBox.Name = "ValueTextBox";
            this.ValueTextBox.Size = new System.Drawing.Size(82, 27);
            this.ValueTextBox.TabIndex = 6;
            // 
            // HeightTextBox
            // 
            this.HeightTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.HeightTextBox.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.HeightTextBox.Location = new System.Drawing.Point(83, 42);
            this.HeightTextBox.Margin = new System.Windows.Forms.Padding(0);
            this.HeightTextBox.Name = "HeightTextBox";
            this.HeightTextBox.Size = new System.Drawing.Size(82, 27);
            this.HeightTextBox.TabIndex = 5;
            // 
            // WidthLabel
            // 
            this.WidthLabel.AutoSize = true;
            this.WidthLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.WidthLabel.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.WidthLabel.Location = new System.Drawing.Point(5, 1);
            this.WidthLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.WidthLabel.Name = "WidthLabel";
            this.WidthLabel.Size = new System.Drawing.Size(73, 36);
            this.WidthLabel.TabIndex = 0;
            this.WidthLabel.Text = "Szerokość";
            this.WidthLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // CountLabel
            // 
            this.CountLabel.AutoSize = true;
            this.CountLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CountLabel.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.CountLabel.Location = new System.Drawing.Point(5, 112);
            this.CountLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.CountLabel.Name = "CountLabel";
            this.CountLabel.Size = new System.Drawing.Size(73, 39);
            this.CountLabel.TabIndex = 3;
            this.CountLabel.Text = "Ilość";
            this.CountLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ValueLabel
            // 
            this.ValueLabel.AutoSize = true;
            this.ValueLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ValueLabel.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.ValueLabel.Location = new System.Drawing.Point(5, 75);
            this.ValueLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.ValueLabel.Name = "ValueLabel";
            this.ValueLabel.Size = new System.Drawing.Size(73, 36);
            this.ValueLabel.TabIndex = 2;
            this.ValueLabel.Text = "Wartość";
            this.ValueLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // HeightLabel
            // 
            this.HeightLabel.AutoSize = true;
            this.HeightLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.HeightLabel.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.HeightLabel.Location = new System.Drawing.Point(5, 38);
            this.HeightLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.HeightLabel.Name = "HeightLabel";
            this.HeightLabel.Size = new System.Drawing.Size(73, 36);
            this.HeightLabel.TabIndex = 1;
            this.HeightLabel.Text = "Wysokość";
            this.HeightLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // WidthTextBox
            // 
            this.WidthTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.WidthTextBox.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.WidthTextBox.Location = new System.Drawing.Point(83, 5);
            this.WidthTextBox.Margin = new System.Windows.Forms.Padding(0);
            this.WidthTextBox.Name = "WidthTextBox";
            this.WidthTextBox.Size = new System.Drawing.Size(82, 27);
            this.WidthTextBox.TabIndex = 4;
            // 
            // AddElementButton
            // 
            this.AddElementButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.AddElementButton.Location = new System.Drawing.Point(0, 208);
            this.AddElementButton.Margin = new System.Windows.Forms.Padding(0);
            this.AddElementButton.Name = "AddElementButton";
            this.AddElementButton.Size = new System.Drawing.Size(174, 29);
            this.AddElementButton.TabIndex = 4;
            this.AddElementButton.Text = "Dodaj kawałek";
            this.AddElementButton.UseVisualStyleBackColor = true;
            this.AddElementButton.Click += new System.EventHandler(this.AddElementButton_Click);
            // 
            // SetKnapsackSizeTableLayoutPanel
            // 
            this.SetKnapsackSizeTableLayoutPanel.ColumnCount = 1;
            this.SetKnapsackSizeTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.SetKnapsackSizeTableLayoutPanel.Controls.Add(this.tableLayoutPanel6, 0, 1);
            this.SetKnapsackSizeTableLayoutPanel.Controls.Add(this.SetKnapsackSizeLabel, 0, 0);
            this.SetKnapsackSizeTableLayoutPanel.Controls.Add(this.SetKnapsackSizeButton, 0, 2);
            this.SetKnapsackSizeTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SetKnapsackSizeTableLayoutPanel.Location = new System.Drawing.Point(5, 212);
            this.SetKnapsackSizeTableLayoutPanel.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.SetKnapsackSizeTableLayoutPanel.Name = "SetKnapsackSizeTableLayoutPanel";
            this.SetKnapsackSizeTableLayoutPanel.RowCount = 3;
            this.SetKnapsackSizeTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.SetKnapsackSizeTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.SetKnapsackSizeTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.SetKnapsackSizeTableLayoutPanel.Size = new System.Drawing.Size(166, 142);
            this.SetKnapsackSizeTableLayoutPanel.TabIndex = 4;
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel6.ColumnCount = 2;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.Controls.Add(this.KnapsackHeightTextBox, 1, 1);
            this.tableLayoutPanel6.Controls.Add(this.KnapsackWidthLabel, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.KnapsackHeightLabel, 0, 1);
            this.tableLayoutPanel6.Controls.Add(this.KnapsackWidthTextBox, 1, 0);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(4, 53);
            this.tableLayoutPanel6.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 2;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(158, 57);
            this.tableLayoutPanel6.TabIndex = 5;
            // 
            // KnapsackHeightTextBox
            // 
            this.KnapsackHeightTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.KnapsackHeightTextBox.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.KnapsackHeightTextBox.Location = new System.Drawing.Point(79, 29);
            this.KnapsackHeightTextBox.Margin = new System.Windows.Forms.Padding(0);
            this.KnapsackHeightTextBox.Name = "KnapsackHeightTextBox";
            this.KnapsackHeightTextBox.Size = new System.Drawing.Size(78, 27);
            this.KnapsackHeightTextBox.TabIndex = 5;
            // 
            // KnapsackWidthLabel
            // 
            this.KnapsackWidthLabel.AutoSize = true;
            this.KnapsackWidthLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.KnapsackWidthLabel.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.KnapsackWidthLabel.Location = new System.Drawing.Point(5, 1);
            this.KnapsackWidthLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.KnapsackWidthLabel.Name = "KnapsackWidthLabel";
            this.KnapsackWidthLabel.Size = new System.Drawing.Size(69, 27);
            this.KnapsackWidthLabel.TabIndex = 0;
            this.KnapsackWidthLabel.Text = "Szerokość";
            this.KnapsackWidthLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // KnapsackHeightLabel
            // 
            this.KnapsackHeightLabel.AutoSize = true;
            this.KnapsackHeightLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.KnapsackHeightLabel.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.KnapsackHeightLabel.Location = new System.Drawing.Point(5, 29);
            this.KnapsackHeightLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.KnapsackHeightLabel.Name = "KnapsackHeightLabel";
            this.KnapsackHeightLabel.Size = new System.Drawing.Size(69, 27);
            this.KnapsackHeightLabel.TabIndex = 1;
            this.KnapsackHeightLabel.Text = "Wysokość";
            this.KnapsackHeightLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // KnapsackWidthTextBox
            // 
            this.KnapsackWidthTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.KnapsackWidthTextBox.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.KnapsackWidthTextBox.Location = new System.Drawing.Point(79, 1);
            this.KnapsackWidthTextBox.Margin = new System.Windows.Forms.Padding(0);
            this.KnapsackWidthTextBox.Name = "KnapsackWidthTextBox";
            this.KnapsackWidthTextBox.Size = new System.Drawing.Size(78, 27);
            this.KnapsackWidthTextBox.TabIndex = 4;
            // 
            // SetKnapsackSizeLabel
            // 
            this.SetKnapsackSizeLabel.AutoSize = true;
            this.SetKnapsackSizeLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SetKnapsackSizeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SetKnapsackSizeLabel.Location = new System.Drawing.Point(4, 0);
            this.SetKnapsackSizeLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.SetKnapsackSizeLabel.Name = "SetKnapsackSizeLabel";
            this.SetKnapsackSizeLabel.Size = new System.Drawing.Size(158, 50);
            this.SetKnapsackSizeLabel.TabIndex = 3;
            this.SetKnapsackSizeLabel.Text = "Ustaw rozmiary plecaka";
            this.SetKnapsackSizeLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // SetKnapsackSizeButton
            // 
            this.SetKnapsackSizeButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SetKnapsackSizeButton.Location = new System.Drawing.Point(0, 113);
            this.SetKnapsackSizeButton.Margin = new System.Windows.Forms.Padding(0);
            this.SetKnapsackSizeButton.Name = "SetKnapsackSizeButton";
            this.SetKnapsackSizeButton.Size = new System.Drawing.Size(166, 29);
            this.SetKnapsackSizeButton.TabIndex = 4;
            this.SetKnapsackSizeButton.Text = "Zapisz";
            this.SetKnapsackSizeButton.UseVisualStyleBackColor = true;
            this.SetKnapsackSizeButton.Click += new System.EventHandler(this.SetKnapsackSizeButton_Click);
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.ColumnCount = 1;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel8.Controls.Add(this.EditElementSaveButton, 0, 2);
            this.tableLayoutPanel8.Controls.Add(this.tableLayoutPanel9, 0, 1);
            this.tableLayoutPanel8.Controls.Add(this.EditElementLabel, 0, 0);
            this.tableLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel8.Location = new System.Drawing.Point(4, 4);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 3;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(168, 201);
            this.tableLayoutPanel8.TabIndex = 5;
            // 
            // EditElementSaveButton
            // 
            this.EditElementSaveButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.EditElementSaveButton.Enabled = false;
            this.EditElementSaveButton.Location = new System.Drawing.Point(0, 171);
            this.EditElementSaveButton.Margin = new System.Windows.Forms.Padding(0);
            this.EditElementSaveButton.Name = "EditElementSaveButton";
            this.EditElementSaveButton.Size = new System.Drawing.Size(168, 30);
            this.EditElementSaveButton.TabIndex = 7;
            this.EditElementSaveButton.Text = "Zapisz";
            this.EditElementSaveButton.UseVisualStyleBackColor = true;
            this.EditElementSaveButton.Click += new System.EventHandler(this.EditElementSaveButton_Click);
            // 
            // tableLayoutPanel9
            // 
            this.tableLayoutPanel9.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel9.ColumnCount = 2;
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel9.Controls.Add(this.EditElementValueTextBox, 1, 2);
            this.tableLayoutPanel9.Controls.Add(this.EditElementValueLabel, 0, 2);
            this.tableLayoutPanel9.Controls.Add(this.EditElementHeightTextBox, 1, 1);
            this.tableLayoutPanel9.Controls.Add(this.EditElementWidthLabel, 0, 0);
            this.tableLayoutPanel9.Controls.Add(this.EditElementHeightLabel, 0, 1);
            this.tableLayoutPanel9.Controls.Add(this.EditElementWidthTextBox, 1, 0);
            this.tableLayoutPanel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel9.Location = new System.Drawing.Point(4, 33);
            this.tableLayoutPanel9.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.tableLayoutPanel9.Name = "tableLayoutPanel9";
            this.tableLayoutPanel9.RowCount = 3;
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel9.Size = new System.Drawing.Size(160, 135);
            this.tableLayoutPanel9.TabIndex = 6;
            // 
            // EditElementValueTextBox
            // 
            this.EditElementValueTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.EditElementValueTextBox.Enabled = false;
            this.EditElementValueTextBox.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.EditElementValueTextBox.Location = new System.Drawing.Point(80, 98);
            this.EditElementValueTextBox.Margin = new System.Windows.Forms.Padding(0);
            this.EditElementValueTextBox.Name = "EditElementValueTextBox";
            this.EditElementValueTextBox.Size = new System.Drawing.Size(79, 27);
            this.EditElementValueTextBox.TabIndex = 7;
            // 
            // EditElementValueLabel
            // 
            this.EditElementValueLabel.AutoSize = true;
            this.EditElementValueLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.EditElementValueLabel.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.EditElementValueLabel.Location = new System.Drawing.Point(5, 89);
            this.EditElementValueLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.EditElementValueLabel.Name = "EditElementValueLabel";
            this.EditElementValueLabel.Size = new System.Drawing.Size(70, 45);
            this.EditElementValueLabel.TabIndex = 6;
            this.EditElementValueLabel.Text = "Wartość";
            this.EditElementValueLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // EditElementHeightTextBox
            // 
            this.EditElementHeightTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.EditElementHeightTextBox.Enabled = false;
            this.EditElementHeightTextBox.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.EditElementHeightTextBox.Location = new System.Drawing.Point(80, 53);
            this.EditElementHeightTextBox.Margin = new System.Windows.Forms.Padding(0);
            this.EditElementHeightTextBox.Name = "EditElementHeightTextBox";
            this.EditElementHeightTextBox.Size = new System.Drawing.Size(79, 27);
            this.EditElementHeightTextBox.TabIndex = 5;
            // 
            // EditElementWidthLabel
            // 
            this.EditElementWidthLabel.AutoSize = true;
            this.EditElementWidthLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.EditElementWidthLabel.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.EditElementWidthLabel.Location = new System.Drawing.Point(5, 1);
            this.EditElementWidthLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.EditElementWidthLabel.Name = "EditElementWidthLabel";
            this.EditElementWidthLabel.Size = new System.Drawing.Size(70, 43);
            this.EditElementWidthLabel.TabIndex = 0;
            this.EditElementWidthLabel.Text = "Szerokość";
            this.EditElementWidthLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // EditElementHeightLabel
            // 
            this.EditElementHeightLabel.AutoSize = true;
            this.EditElementHeightLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.EditElementHeightLabel.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.EditElementHeightLabel.Location = new System.Drawing.Point(5, 45);
            this.EditElementHeightLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.EditElementHeightLabel.Name = "EditElementHeightLabel";
            this.EditElementHeightLabel.Size = new System.Drawing.Size(70, 43);
            this.EditElementHeightLabel.TabIndex = 1;
            this.EditElementHeightLabel.Text = "Wysokość";
            this.EditElementHeightLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // EditElementWidthTextBox
            // 
            this.EditElementWidthTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.EditElementWidthTextBox.Enabled = false;
            this.EditElementWidthTextBox.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.EditElementWidthTextBox.Location = new System.Drawing.Point(80, 9);
            this.EditElementWidthTextBox.Margin = new System.Windows.Forms.Padding(0);
            this.EditElementWidthTextBox.Name = "EditElementWidthTextBox";
            this.EditElementWidthTextBox.Size = new System.Drawing.Size(79, 27);
            this.EditElementWidthTextBox.TabIndex = 4;
            // 
            // EditElementLabel
            // 
            this.EditElementLabel.AutoSize = true;
            this.EditElementLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.EditElementLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.EditElementLabel.Location = new System.Drawing.Point(4, 0);
            this.EditElementLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.EditElementLabel.Name = "EditElementLabel";
            this.EditElementLabel.Size = new System.Drawing.Size(160, 30);
            this.EditElementLabel.TabIndex = 4;
            this.EditElementLabel.Text = "Edytuj kawałek";
            this.EditElementLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ScoreTableLayoutPanel
            // 
            this.ScoreTableLayoutPanel.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.ScoreTableLayoutPanel.ColumnCount = 1;
            this.ScoreTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.ScoreTableLayoutPanel.Controls.Add(this.tableLayoutPanel4, 0, 1);
            this.ScoreTableLayoutPanel.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.ScoreTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ScoreTableLayoutPanel.Location = new System.Drawing.Point(1002, 1);
            this.ScoreTableLayoutPanel.Margin = new System.Windows.Forms.Padding(0);
            this.ScoreTableLayoutPanel.Name = "ScoreTableLayoutPanel";
            this.ScoreTableLayoutPanel.RowCount = 2;
            this.ScoreTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.ScoreTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 65F));
            this.ScoreTableLayoutPanel.Size = new System.Drawing.Size(177, 596);
            this.ScoreTableLayoutPanel.TabIndex = 2;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 1;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Controls.Add(this.KnapsackInfoLabel, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.tableLayoutPanel5, 0, 1);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(1, 209);
            this.tableLayoutPanel4.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 2;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(175, 386);
            this.tableLayoutPanel4.TabIndex = 5;
            // 
            // KnapsackInfoLabel
            // 
            this.KnapsackInfoLabel.AutoSize = true;
            this.KnapsackInfoLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.KnapsackInfoLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.KnapsackInfoLabel.Location = new System.Drawing.Point(4, 0);
            this.KnapsackInfoLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.KnapsackInfoLabel.Name = "KnapsackInfoLabel";
            this.KnapsackInfoLabel.Size = new System.Drawing.Size(167, 29);
            this.KnapsackInfoLabel.TabIndex = 2;
            this.KnapsackInfoLabel.Text = "Informacje o plecaku";
            this.KnapsackInfoLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel5.ColumnCount = 2;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel5.Controls.Add(this.KnapsackSizeValueLabel, 1, 0);
            this.tableLayoutPanel5.Controls.Add(this.KnapsackElementsCountValueLabel, 1, 4);
            this.tableLayoutPanel5.Controls.Add(this.KnapsackValueValueLabel, 1, 3);
            this.tableLayoutPanel5.Controls.Add(this.UsedSpaceValueLabel, 1, 2);
            this.tableLayoutPanel5.Controls.Add(this.KnapsackSpaceLabel, 0, 1);
            this.tableLayoutPanel5.Controls.Add(this.KnapsackElementsCountLabel, 0, 4);
            this.tableLayoutPanel5.Controls.Add(this.KnapsackValueLabel, 0, 3);
            this.tableLayoutPanel5.Controls.Add(this.UsedSpaceLabel, 0, 2);
            this.tableLayoutPanel5.Controls.Add(this.KnapsackSpaceValueLabel, 1, 1);
            this.tableLayoutPanel5.Controls.Add(this.KnapsackSizeLabel, 0, 0);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(4, 32);
            this.tableLayoutPanel5.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 5;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(167, 351);
            this.tableLayoutPanel5.TabIndex = 3;
            // 
            // KnapsackSizeValueLabel
            // 
            this.KnapsackSizeValueLabel.AutoSize = true;
            this.KnapsackSizeValueLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.KnapsackSizeValueLabel.Location = new System.Drawing.Point(100, 1);
            this.KnapsackSizeValueLabel.Margin = new System.Windows.Forms.Padding(0);
            this.KnapsackSizeValueLabel.Name = "KnapsackSizeValueLabel";
            this.KnapsackSizeValueLabel.Size = new System.Drawing.Size(66, 69);
            this.KnapsackSizeValueLabel.TabIndex = 9;
            this.KnapsackSizeValueLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // KnapsackElementsCountValueLabel
            // 
            this.KnapsackElementsCountValueLabel.AutoSize = true;
            this.KnapsackElementsCountValueLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.KnapsackElementsCountValueLabel.Location = new System.Drawing.Point(104, 281);
            this.KnapsackElementsCountValueLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.KnapsackElementsCountValueLabel.Name = "KnapsackElementsCountValueLabel";
            this.KnapsackElementsCountValueLabel.Size = new System.Drawing.Size(58, 69);
            this.KnapsackElementsCountValueLabel.TabIndex = 7;
            this.KnapsackElementsCountValueLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // KnapsackValueValueLabel
            // 
            this.KnapsackValueValueLabel.AutoSize = true;
            this.KnapsackValueValueLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.KnapsackValueValueLabel.Location = new System.Drawing.Point(104, 211);
            this.KnapsackValueValueLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.KnapsackValueValueLabel.Name = "KnapsackValueValueLabel";
            this.KnapsackValueValueLabel.Size = new System.Drawing.Size(58, 69);
            this.KnapsackValueValueLabel.TabIndex = 6;
            this.KnapsackValueValueLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // UsedSpaceValueLabel
            // 
            this.UsedSpaceValueLabel.AutoSize = true;
            this.UsedSpaceValueLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.UsedSpaceValueLabel.Location = new System.Drawing.Point(104, 141);
            this.UsedSpaceValueLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.UsedSpaceValueLabel.Name = "UsedSpaceValueLabel";
            this.UsedSpaceValueLabel.Size = new System.Drawing.Size(58, 69);
            this.UsedSpaceValueLabel.TabIndex = 5;
            this.UsedSpaceValueLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // KnapsackSpaceLabel
            // 
            this.KnapsackSpaceLabel.AutoSize = true;
            this.KnapsackSpaceLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.KnapsackSpaceLabel.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.KnapsackSpaceLabel.Location = new System.Drawing.Point(5, 71);
            this.KnapsackSpaceLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.KnapsackSpaceLabel.Name = "KnapsackSpaceLabel";
            this.KnapsackSpaceLabel.Size = new System.Drawing.Size(90, 69);
            this.KnapsackSpaceLabel.TabIndex = 0;
            this.KnapsackSpaceLabel.Text = "Miejsce w plecaku";
            this.KnapsackSpaceLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // KnapsackElementsCountLabel
            // 
            this.KnapsackElementsCountLabel.AutoSize = true;
            this.KnapsackElementsCountLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.KnapsackElementsCountLabel.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.KnapsackElementsCountLabel.Location = new System.Drawing.Point(5, 281);
            this.KnapsackElementsCountLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.KnapsackElementsCountLabel.Name = "KnapsackElementsCountLabel";
            this.KnapsackElementsCountLabel.Size = new System.Drawing.Size(90, 69);
            this.KnapsackElementsCountLabel.TabIndex = 3;
            this.KnapsackElementsCountLabel.Text = "Liczba kawałków w plecaku";
            this.KnapsackElementsCountLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // KnapsackValueLabel
            // 
            this.KnapsackValueLabel.AutoSize = true;
            this.KnapsackValueLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.KnapsackValueLabel.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.KnapsackValueLabel.Location = new System.Drawing.Point(5, 211);
            this.KnapsackValueLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.KnapsackValueLabel.Name = "KnapsackValueLabel";
            this.KnapsackValueLabel.Size = new System.Drawing.Size(90, 69);
            this.KnapsackValueLabel.TabIndex = 2;
            this.KnapsackValueLabel.Text = "Wartość plecaka";
            this.KnapsackValueLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // UsedSpaceLabel
            // 
            this.UsedSpaceLabel.AutoSize = true;
            this.UsedSpaceLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.UsedSpaceLabel.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.UsedSpaceLabel.Location = new System.Drawing.Point(5, 141);
            this.UsedSpaceLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.UsedSpaceLabel.Name = "UsedSpaceLabel";
            this.UsedSpaceLabel.Size = new System.Drawing.Size(90, 69);
            this.UsedSpaceLabel.TabIndex = 1;
            this.UsedSpaceLabel.Text = "Zajęte miejsce";
            this.UsedSpaceLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // KnapsackSpaceValueLabel
            // 
            this.KnapsackSpaceValueLabel.AutoSize = true;
            this.KnapsackSpaceValueLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.KnapsackSpaceValueLabel.Location = new System.Drawing.Point(104, 71);
            this.KnapsackSpaceValueLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.KnapsackSpaceValueLabel.Name = "KnapsackSpaceValueLabel";
            this.KnapsackSpaceValueLabel.Size = new System.Drawing.Size(58, 69);
            this.KnapsackSpaceValueLabel.TabIndex = 4;
            this.KnapsackSpaceValueLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // KnapsackSizeLabel
            // 
            this.KnapsackSizeLabel.AutoSize = true;
            this.KnapsackSizeLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.KnapsackSizeLabel.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.KnapsackSizeLabel.Location = new System.Drawing.Point(5, 1);
            this.KnapsackSizeLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.KnapsackSizeLabel.Name = "KnapsackSizeLabel";
            this.KnapsackSizeLabel.Size = new System.Drawing.Size(90, 69);
            this.KnapsackSizeLabel.TabIndex = 8;
            this.KnapsackSizeLabel.Text = "Rozmiar plecaka";
            this.KnapsackSizeLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.ElementInfoLabel, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel3, 0, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(1, 1);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(175, 207);
            this.tableLayoutPanel2.TabIndex = 4;
            // 
            // ElementInfoLabel
            // 
            this.ElementInfoLabel.AutoSize = true;
            this.ElementInfoLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ElementInfoLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ElementInfoLabel.Location = new System.Drawing.Point(4, 0);
            this.ElementInfoLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.ElementInfoLabel.Name = "ElementInfoLabel";
            this.ElementInfoLabel.Size = new System.Drawing.Size(167, 29);
            this.ElementInfoLabel.TabIndex = 2;
            this.ElementInfoLabel.Text = "Informacje o kawałku";
            this.ElementInfoLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel3.Controls.Add(this.InfoValueValueLabel, 1, 2);
            this.tableLayoutPanel3.Controls.Add(this.InfoHeightValueLabel, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.InfoWidthLabel, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.InfoValueLabel, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.InfoHeightLabel, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.InfoWidthValueLabel, 1, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(4, 32);
            this.tableLayoutPanel3.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 3;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(167, 172);
            this.tableLayoutPanel3.TabIndex = 3;
            // 
            // InfoValueValueLabel
            // 
            this.InfoValueValueLabel.AutoSize = true;
            this.InfoValueValueLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.InfoValueValueLabel.Location = new System.Drawing.Point(104, 115);
            this.InfoValueValueLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.InfoValueValueLabel.Name = "InfoValueValueLabel";
            this.InfoValueValueLabel.Size = new System.Drawing.Size(58, 56);
            this.InfoValueValueLabel.TabIndex = 6;
            this.InfoValueValueLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // InfoHeightValueLabel
            // 
            this.InfoHeightValueLabel.AutoSize = true;
            this.InfoHeightValueLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.InfoHeightValueLabel.Location = new System.Drawing.Point(104, 58);
            this.InfoHeightValueLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.InfoHeightValueLabel.Name = "InfoHeightValueLabel";
            this.InfoHeightValueLabel.Size = new System.Drawing.Size(58, 56);
            this.InfoHeightValueLabel.TabIndex = 5;
            this.InfoHeightValueLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // InfoWidthLabel
            // 
            this.InfoWidthLabel.AutoSize = true;
            this.InfoWidthLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.InfoWidthLabel.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.InfoWidthLabel.Location = new System.Drawing.Point(5, 1);
            this.InfoWidthLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.InfoWidthLabel.Name = "InfoWidthLabel";
            this.InfoWidthLabel.Size = new System.Drawing.Size(90, 56);
            this.InfoWidthLabel.TabIndex = 0;
            this.InfoWidthLabel.Text = "Szerokość";
            this.InfoWidthLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // InfoValueLabel
            // 
            this.InfoValueLabel.AutoSize = true;
            this.InfoValueLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.InfoValueLabel.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.InfoValueLabel.Location = new System.Drawing.Point(5, 115);
            this.InfoValueLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.InfoValueLabel.Name = "InfoValueLabel";
            this.InfoValueLabel.Size = new System.Drawing.Size(90, 56);
            this.InfoValueLabel.TabIndex = 2;
            this.InfoValueLabel.Text = "Wartość";
            this.InfoValueLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // InfoHeightLabel
            // 
            this.InfoHeightLabel.AutoSize = true;
            this.InfoHeightLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.InfoHeightLabel.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.InfoHeightLabel.Location = new System.Drawing.Point(5, 58);
            this.InfoHeightLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.InfoHeightLabel.Name = "InfoHeightLabel";
            this.InfoHeightLabel.Size = new System.Drawing.Size(90, 56);
            this.InfoHeightLabel.TabIndex = 1;
            this.InfoHeightLabel.Text = "Wysokość";
            this.InfoHeightLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // InfoWidthValueLabel
            // 
            this.InfoWidthValueLabel.AutoSize = true;
            this.InfoWidthValueLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.InfoWidthValueLabel.Location = new System.Drawing.Point(104, 1);
            this.InfoWidthValueLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.InfoWidthValueLabel.Name = "InfoWidthValueLabel";
            this.InfoWidthValueLabel.Size = new System.Drawing.Size(58, 56);
            this.InfoWidthValueLabel.TabIndex = 4;
            this.InfoWidthValueLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel7.ColumnCount = 1;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel7.Controls.Add(this.AllElementsFlowLayoutPanel, 0, 0);
            this.tableLayoutPanel7.Controls.Add(this.KnapsackPictureBox, 0, 1);
            this.tableLayoutPanel7.Controls.Add(this.StartTest, 0, 2);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel7.Location = new System.Drawing.Point(181, 4);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 3;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 45F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 55F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(817, 590);
            this.tableLayoutPanel7.TabIndex = 3;
            // 
            // AllElementsFlowLayoutPanel
            // 
            this.AllElementsFlowLayoutPanel.AutoScroll = true;
            this.AllElementsFlowLayoutPanel.AutoSize = true;
            this.AllElementsFlowLayoutPanel.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.AllElementsFlowLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.AllElementsFlowLayoutPanel.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.AllElementsFlowLayoutPanel.Location = new System.Drawing.Point(1, 1);
            this.AllElementsFlowLayoutPanel.Margin = new System.Windows.Forms.Padding(0);
            this.AllElementsFlowLayoutPanel.Name = "AllElementsFlowLayoutPanel";
            this.AllElementsFlowLayoutPanel.Size = new System.Drawing.Size(815, 245);
            this.AllElementsFlowLayoutPanel.TabIndex = 0;
            // 
            // KnapsackPictureBox
            // 
            this.KnapsackPictureBox.BackColor = System.Drawing.Color.White;
            this.KnapsackPictureBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.KnapsackPictureBox.Location = new System.Drawing.Point(3, 249);
            this.KnapsackPictureBox.Margin = new System.Windows.Forms.Padding(2);
            this.KnapsackPictureBox.Name = "KnapsackPictureBox";
            this.KnapsackPictureBox.Size = new System.Drawing.Size(811, 296);
            this.KnapsackPictureBox.TabIndex = 1;
            this.KnapsackPictureBox.TabStop = false;
            // 
            // StartTest
            // 
            this.StartTest.Dock = System.Windows.Forms.DockStyle.Fill;
            this.StartTest.Location = new System.Drawing.Point(4, 551);
            this.StartTest.Name = "StartTest";
            this.StartTest.Size = new System.Drawing.Size(809, 35);
            this.StartTest.TabIndex = 2;
            this.StartTest.Text = "Szukaj najlepszego dopasowania";
            this.StartTest.UseVisualStyleBackColor = true;
            this.StartTest.Click += new System.EventHandler(this.StartTest_Click);
            // 
            // plikToolStripMenuItem
            // 
            this.plikToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.wczytajTestToolStripMenuItem,
            this.zapiszToolStripMenuItem});
            this.plikToolStripMenuItem.Name = "plikToolStripMenuItem";
            this.plikToolStripMenuItem.Size = new System.Drawing.Size(38, 20);
            this.plikToolStripMenuItem.Text = "Plik";
            // 
            // wczytajTestToolStripMenuItem
            // 
            this.wczytajTestToolStripMenuItem.Name = "wczytajTestToolStripMenuItem";
            this.wczytajTestToolStripMenuItem.Size = new System.Drawing.Size(115, 22);
            this.wczytajTestToolStripMenuItem.Text = "Wczytaj";
            this.wczytajTestToolStripMenuItem.Click += new System.EventHandler(this.wczytajTestToolStripMenuItem_Click);
            // 
            // zapiszToolStripMenuItem
            // 
            this.zapiszToolStripMenuItem.Name = "zapiszToolStripMenuItem";
            this.zapiszToolStripMenuItem.Size = new System.Drawing.Size(115, 22);
            this.zapiszToolStripMenuItem.Text = "Zapisz";
            this.zapiszToolStripMenuItem.Click += new System.EventHandler(this.zapiszToolStripMenuItem_Click);
            // 
            // DeleteAllElementsStripMenuItem
            // 
            this.DeleteAllElementsStripMenuItem.Name = "DeleteAllElementsStripMenuItem";
            this.DeleteAllElementsStripMenuItem.Size = new System.Drawing.Size(132, 20);
            this.DeleteAllElementsStripMenuItem.Text = "Usuń wszystkie klocki";
            this.DeleteAllElementsStripMenuItem.Click += new System.EventHandler(this.DeleteAllElementsToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1180, 624);
            this.Controls.Add(this.rootLayoutPanel);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Problem Plecakowy";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.keyDown);
            this.rootLayoutPanel.ResumeLayout(false);
            this.rootLayoutPanel.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.tablePanel2.ResumeLayout(false);
            this.ElementsTablePanel.ResumeLayout(false);
            this.AddElementTableLayoutPanel.ResumeLayout(false);
            this.AddElementTableLayoutPanel.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.SetKnapsackSizeTableLayoutPanel.ResumeLayout(false);
            this.SetKnapsackSizeTableLayoutPanel.PerformLayout();
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel6.PerformLayout();
            this.tableLayoutPanel8.ResumeLayout(false);
            this.tableLayoutPanel8.PerformLayout();
            this.tableLayoutPanel9.ResumeLayout(false);
            this.tableLayoutPanel9.PerformLayout();
            this.ScoreTableLayoutPanel.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tableLayoutPanel7.ResumeLayout(false);
            this.tableLayoutPanel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.KnapsackPictureBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel rootLayoutPanel;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem plikToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem wczytajTestToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem zapiszToolStripMenuItem;
        private System.Windows.Forms.TableLayoutPanel tablePanel2;
        private System.Windows.Forms.TableLayoutPanel ElementsTablePanel;
        private System.Windows.Forms.FlowLayoutPanel AllElementsFlowLayoutPanel;
        private System.Windows.Forms.Label AddElementsLabel;
        private System.Windows.Forms.PictureBox KnapsackPictureBox;
        private System.Windows.Forms.TableLayoutPanel AddElementTableLayoutPanel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label WidthLabel;
        private System.Windows.Forms.Label CountLabel;
        private System.Windows.Forms.Label ValueLabel;
        private System.Windows.Forms.Label HeightLabel;
        private System.Windows.Forms.TextBox WidthTextBox;
        private System.Windows.Forms.TextBox CountTextBox;
        private System.Windows.Forms.TextBox ValueTextBox;
        private System.Windows.Forms.TextBox HeightTextBox;
        private System.Windows.Forms.Button AddElementButton;
        private System.Windows.Forms.TableLayoutPanel ScoreTableLayoutPanel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label ElementInfoLabel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Label InfoWidthLabel;
        private System.Windows.Forms.Label InfoValueLabel;
        private System.Windows.Forms.Label InfoHeightLabel;
        private System.Windows.Forms.Label InfoValueValueLabel;
        private System.Windows.Forms.Label InfoHeightValueLabel;
        private System.Windows.Forms.Label InfoWidthValueLabel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Label KnapsackInfoLabel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.Label KnapsackElementsCountValueLabel;
        private System.Windows.Forms.Label KnapsackValueValueLabel;
        private System.Windows.Forms.Label UsedSpaceValueLabel;
        private System.Windows.Forms.Label KnapsackSpaceLabel;
        private System.Windows.Forms.Label KnapsackElementsCountLabel;
        private System.Windows.Forms.Label KnapsackValueLabel;
        private System.Windows.Forms.Label UsedSpaceLabel;
        private System.Windows.Forms.Label KnapsackSpaceValueLabel;
        private System.Windows.Forms.ToolStripMenuItem DeleteAllElementsStripMenuItem;
        private System.Windows.Forms.Label KnapsackSizeLabel;
        private System.Windows.Forms.Label KnapsackSizeValueLabel;
        private System.Windows.Forms.TableLayoutPanel SetKnapsackSizeTableLayoutPanel;
        private System.Windows.Forms.Label SetKnapsackSizeLabel;
        private System.Windows.Forms.Button SetKnapsackSizeButton;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.TextBox KnapsackHeightTextBox;
        private System.Windows.Forms.Label KnapsackWidthLabel;
        private System.Windows.Forms.Label KnapsackHeightLabel;
        private System.Windows.Forms.TextBox KnapsackWidthTextBox;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.Button StartTest;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel9;
        private System.Windows.Forms.TextBox EditElementHeightTextBox;
        private System.Windows.Forms.Label EditElementWidthLabel;
        private System.Windows.Forms.Label EditElementHeightLabel;
        private System.Windows.Forms.TextBox EditElementWidthTextBox;
        private System.Windows.Forms.Label EditElementLabel;
        private System.Windows.Forms.Button EditElementSaveButton;
        private System.Windows.Forms.TextBox EditElementValueTextBox;
        private System.Windows.Forms.Label EditElementValueLabel;
        private System.Windows.Forms.ToolStripMenuItem plikToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem wczytajToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem zapiszToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem usuńWszystkieKlockiToolStripMenuItem;
    }
}

