﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KnapsackProblem.UI.Models
{
    public class ElementInfo
    {
        public int Width { get; set; }

        public int Height { get; set; }

        public int Value { get; set; }

        public int Id { get; set; }

        public Point Position { get; set; }
    }
}
