﻿using KnapsackProblem.UI.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KnapsackProblem.UI.Models
{
    public class InputInfo
    {
        public int BoardWidth { get; set; }

        public int BoardHeight { get; set; }

        public List<ElementInfo> Elements{ get; set; }
    }
}
