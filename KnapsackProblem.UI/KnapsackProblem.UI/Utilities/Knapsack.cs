﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KnapsackProblem.UI.Utilities
{
    public class Knapsack
    {
        public List<Element> allElementsAvailable;
        public Bitmap Picture { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public int PixelsPerUnit { get; set; }

        private Graphics imageGraphics;



        public Knapsack(int Width, int Height, Size pictureSize, List<Element> allElementsAvailable, int pixelsPerUnit)
        {
            this.Width = Width;
            this.Height = Height;
            this.PixelsPerUnit = pixelsPerUnit;
            Picture = new Bitmap(pictureSize.Width, pictureSize.Height);
            this.allElementsAvailable = allElementsAvailable;
            imageGraphics = Graphics.FromImage(Picture);
        }

        public void GeneratePicture(List<Element> Elements, List<Point> Points)
        {

            imageGraphics.Clear(Color.White);
            DrawGrid();
            for (int i = 0; i < Elements.Count; i++)
            {
                Element currentElement = Elements[i];
                Point currentPoint = Points[i];

                DrawElement(currentElement, currentPoint);
            }
        }

        private void DrawElement(Element currentElement, Point currentPoint)
        {
            imageGraphics.DrawImage(currentElement.View, new Point(currentPoint.X*PixelsPerUnit, currentPoint.Y*PixelsPerUnit));
        }

        public void DrawGrid()
        {
            imageGraphics.Clear(Color.White);
            Pen pen = new Pen(new SolidBrush(Color.Black), 1);
            int i = 0;
            while (i < Width + 1)
            {
                imageGraphics.DrawLine(pen, new Point(i * PixelsPerUnit, 0), new Point(i * PixelsPerUnit, Height * PixelsPerUnit));
                i += 1;
            }
            i = 0;
            while (i < Height + 1)
            {
                imageGraphics.DrawLine(pen, new Point(0, i * PixelsPerUnit), new Point(Width * PixelsPerUnit, i * PixelsPerUnit));
                i += 1;
            }
        }



    }
}
