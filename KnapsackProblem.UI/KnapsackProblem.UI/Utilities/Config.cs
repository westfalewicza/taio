﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KnapsackProblem.UI.Utilities
{
    public static class Config
    {
        public static int WidthTreshold = 100;
        public static int HeightTreshold = 30;
        public static int RescaleFactor = 2;
    }
}
