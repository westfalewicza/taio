﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KnapsackProblem.UI.Utilities
{
    public class Element
    {
        public static int elementCount = 0;
        public int ID { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public int Value { get; set; }
        public Bitmap View { get => view; }
        public Color Color { get; set; }
        public int PixelPerUnit
        {
            get { return pixelsPerUnit; }
            set
            {
                pixelsPerUnit = value;
                GenerateView();
            }
        }

        public bool IsInKnapsack { get; set; }

        private Bitmap view;
        private int pixelsPerUnit;

        public Element(int width, int height, int value, Color color, int pixelsPerUnit)
        {
            Width = width;
            Height = height;
            Value = value;
            this.Color = color;
            this.pixelsPerUnit = pixelsPerUnit;
            GenerateView();
            ID = elementCount;
            elementCount++;
        }

        private void GenerateView()
        {
            view = new Bitmap(Width * pixelsPerUnit, Height * pixelsPerUnit);
            Graphics graphics = Graphics.FromImage(view);
            graphics.FillRectangle(new SolidBrush(this.Color), 0, 0, Width * pixelsPerUnit, Height * pixelsPerUnit);
            if (IsInKnapsack)
                graphics.DrawRectangle(new Pen(new SolidBrush(Color.White), 9), 0, 0, Width * pixelsPerUnit, Height * pixelsPerUnit);
            else
                graphics.DrawRectangle(new Pen(new SolidBrush(Color.Black), 3), 0, 0, Width * pixelsPerUnit, Height * pixelsPerUnit);
            graphics.Dispose();
        }

        public override string ToString()
        {
            return Width.ToString() + " " + Height.ToString() + " " + Value.ToString();
        }

        internal void Refresh()
        {
            GenerateView();
        }
    }
}
