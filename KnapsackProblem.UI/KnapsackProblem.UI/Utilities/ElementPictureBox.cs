﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KnapsackProblem.UI.Utilities
{
    public class ElementPictureBox : PictureBox
    {
        public ElementPictureBox(Element element, int pixelsPerUnit)
        {
            this.element = element;
            Image = element.View.Clone() as Image;
            Size = new Size(element.Width * pixelsPerUnit, element.Height * pixelsPerUnit);
        }

        public Element element { get; set; }

        public bool InKnapsack { get; set; }
        

        

        internal void DrawSelect()
        {
            Graphics.FromImage(Image).DrawRectangle(new Pen(new SolidBrush(Color.Red), 4),0,0,Image.Width, Image.Height);
            Refresh();
        }


    }
}
