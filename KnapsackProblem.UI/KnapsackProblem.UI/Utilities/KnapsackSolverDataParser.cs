﻿using KnapsackProblem.UI.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KnapsackProblem.UI.Utilities
{
    public static class KnapsackSolverDataParser
    {
        public static string ParseInputInfoToString(InputInfo input)
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.AppendLine($"{input.BoardWidth} {input.BoardHeight}");
            stringBuilder.AppendLine($"{input.Elements.Count}");
            foreach (var element in input.Elements.OrderBy(el => el.Id))
            {
                stringBuilder.AppendLine($"{element.Width} {element.Height} {element.Value}");
            }

            return stringBuilder.ToString();
        }

        public static OutputInfo ParseStringOutputToOutputInfo(string outputString)
        {
            var elements = new List<ElementInfo>();

            foreach(var line in outputString.Split(Environment.NewLine.ToCharArray()))
            {
                var data = line.Split(new[] { ',', ':' });

                if (data.Length != 6)
                    continue;

                elements.Add(new ElementInfo()
                {
                    Position = new Point(int.Parse(data[0].Trim()), int.Parse(data[1].Trim())),
                    Id = int.Parse(data[2].Trim()),
                    Width = int.Parse(data[3].Trim()),
                    Height = int.Parse(data[4].Trim()),
                    Value = int.Parse(data[5].Trim())
                });
            }

            return new OutputInfo()
            {
                Elements = elements,
                KnapsackValue = elements.Sum(el => el.Value)
            };
        }
    }
}
