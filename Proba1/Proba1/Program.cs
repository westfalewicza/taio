﻿using System;
using System.Collections.Generic;

namespace Proba1
{
    class Program
    {
        private static Board board;

        private static List<Piece> pieces = new List<Piece>();

        static void Main(string[] args)
        {
            var dimensions = Console.ReadLine().Split();
            int.TryParse(dimensions[0], out int W);
            int.TryParse(dimensions[1], out int H);

            board = new Board(W, H);

            int.TryParse(Console.ReadLine(), out int piecesCount);
            for (int i=0; i<piecesCount; i++)
            {
                var pieceInfo = Console.ReadLine().Split();
                var piece = new Piece
                {
                    Width = int.Parse(pieceInfo[0]),
                    Height = int.Parse(pieceInfo[1]),
                    Value = int.Parse(pieceInfo[2]),
                    IsRotated = false
                };
                pieces.Add(piece);
            }

            var solver = new Solver(board, pieces);
            int result = solver.Solve();

            Console.WriteLine(result.ToString());
            Console.ReadKey();
        }
    }
}
