﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Proba1
{
    public class Board
    {
        public int Width { get; private set; }

        public int Height { get; private set; }

        public Piece[,] Tiles { get; private set; }

        public Board(int W, int H)
        {
            Tiles = new Piece[W, H];
            Width = W;
            Height = H;
        }
    }
}
