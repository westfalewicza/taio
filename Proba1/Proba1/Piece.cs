﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Proba1
{
    public class Piece
    {
        public int Value { get; set; }

        public int Width { get; set; }

        public int Height { get; set; }

        public bool IsRotated { get; set; }

        public bool IsOnBoard { get; set; }
    }
}
