﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Proba1
{
    public class Solver
    {
        private Board board;

        private List<Piece> pieces;

        private List<int> cuttingX = new List<int>();

        private List<int> cuttingY = new List<int>();

        public Solver(Board board, List<Piece> pieces)
        {
            this.board = board;
            this.pieces = pieces;
            PrepareCuttingPoints();
        }

        public int Solve()
        {
            int result = 0;
            foreach (var x in cuttingX)
            {
                foreach (var y in cuttingY)
                {
                    if (board.Tiles[x, y] is null)
                    {
                        foreach (var piece in pieces)
                        {
                            // Not rotated
                            if (!piece.IsOnBoard && (piece.Width + x <= board.Width) && (piece.Height + y <= board.Height))
                            {
                                bool available = true;
                                for (int i = x; (i < x + piece.Width) && available; i++)
                                {
                                    for (int j = y; j < y + piece.Height; j++)
                                    {
                                        if (board.Tiles[i, j] != null)
                                        {
                                            available = false;
                                            break;
                                        }
                                    }
                                }
                                if (!available)
                                    continue;

                                for (int i = x; i < x + piece.Width; i++)
                                {
                                    for (int j = y; j < y + piece.Height; j++)
                                    {
                                        board.Tiles[i, j] = piece;
                                    }
                                }

                                piece.IsOnBoard = true;
                                result = Math.Max(result, Solve());
                                piece.IsOnBoard = false;

                                for (int i = x; i < x + piece.Width; i++)
                                {
                                    for (int j = y; j < y + piece.Height; j++)
                                    {
                                        board.Tiles[i, j] = null;
                                    }
                                }
                            }

                            // Rotated
                            if (!piece.IsOnBoard && (piece.Height + x <= board.Width) && (piece.Width + y <= board.Height))
                            {
                                bool available = true;
                                for (int i = x; (i < x + piece.Height) && available; i++)
                                {
                                    for (int j = y; j < y + piece.Width; j++)
                                    {
                                        if (board.Tiles[i, j] != null)
                                        {
                                            available = false;
                                            break;
                                        }
                                    }
                                }
                                if (!available)
                                    continue;

                                for (int i = x; i < x + piece.Height; i++)
                                {
                                    for (int j = y; j < y + piece.Width; j++)
                                    {
                                        board.Tiles[i, j] = piece;
                                    }
                                }

                                piece.IsOnBoard = true;
                                piece.IsRotated = true;
                                result = Math.Max(result, Solve());
                                piece.IsOnBoard = false;
                                piece.IsRotated = false;

                                for (int i = x; i < x + piece.Height; i++)
                                {
                                    for (int j = y; j < y + piece.Width; j++)
                                    {
                                        board.Tiles[i, j] = null;
                                    }
                                }
                            }
                        }
                    }
                }
            }


            if (result != 0)
                return result;

            foreach (var piece in pieces)
            {
                result += piece.IsOnBoard ? piece.Value : 0;
            }
            return result;
        }

        private void PrepareCuttingPoints()
        {
            cuttingX.Add(0);
            cuttingY.Add(0);

            foreach (var piece in pieces)
            {
                var XSize = cuttingX.Count;
                var YSize = cuttingY.Count;

                for (int i = 0; i < XSize; i++)
                {
                    if (cuttingX[i] + piece.Width < board.Width)
                        cuttingX.Add(cuttingX[i] + piece.Width);
                }

                for (int i = 0; i < YSize; i++)
                {
                    if (cuttingY[i] + piece.Height < board.Height)
                        cuttingY.Add(cuttingY[i] + piece.Height);
                }
            }
        }
    }
}