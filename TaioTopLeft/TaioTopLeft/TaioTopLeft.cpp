// test.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <vector>
#include <utility>
#include <chrono>
using namespace std;

struct Item
{
	int Id;
	int DimensionY;
	int DimensionX;
	int Value;
};
struct Point
{
	int X;
	int Y;
};

struct Pivot {
	Point Point;
	bool Available;
};
struct Block
{
	Item Item;
	bool Available;
	bool Rotated;
};


void processPlacement(vector<Pivot> &pivots, int p, vector<Block> &blockLeft, int b);
void solve(int Height, int Length, vector<Item> &items);
void doSolve(vector<Pivot> &pivots, vector<Block> &itemsLeft);
bool intersectsELementsOrBoundary(const Point& pivot, const Item& item);
void printSolution(const vector<pair<Pivot, Block>> &solution);
void setFlags(const Point &pivot, const Item &item, bool value);
int topMostAtX(int x, int startAtY);
int leftMostAtY(int y, int startAtX);


//Globals
vector<pair<Pivot, Block>> BestSolution;
int BestSolutionValue;

vector<pair<Pivot, Block>> CurrentSolution;
int CurrentSolutionValue;
int available_blocks;

bool* board;
int board_Length;
int board_Height;

int main()
{
	vector<Item> items;
	int boardWidth, boardHeight, n;
	cin >> boardWidth >> boardHeight;
	cin >> n;
	for (int i = 0; i < n; i++)
	{
		int w, h, v;
		cin >> w >> h >> v;

		Item item;
		item.DimensionX = w;
		item.DimensionY = h;
		item.Value = v;
		item.Id = i;
		items.push_back(item);
	}
	//auto s = chrono::system_clock::now();
	solve(boardHeight, boardWidth, items);
	//auto s2 = chrono::system_clock::now();
	printSolution(BestSolution);
	//auto xd = (chrono::time_point_cast<chrono::milliseconds>(s2) - chrono::time_point_cast<chrono::milliseconds>(s));
	//cout << "TIME:" << xd.count();
	exit(0);
}


void solve(int Height, int Length, vector<Item> &items) {
	board_Length = Length;
	board_Height = Height;
	CurrentSolutionValue = 0;
	board = new bool[board_Length * board_Height];
	for (int i = 0; i < board_Length * board_Height; i++)
	{
		board[i] = false;
	}

	vector<Block> blocks;
	for (auto item : items)
	{
		if ((item.DimensionX > board_Height && item.DimensionY > board_Length) ||
			(item.DimensionX > board_Length && item.DimensionY > board_Height) ||
			(item.DimensionX > board_Length && item.DimensionX > board_Height) ||
			(item.DimensionY > board_Length && item.DimensionY > board_Height))
			continue;

		Block b;
		b.Available = true;
		b.Item = item;
		b.Rotated = false;
		blocks.push_back(b);
	}
	
	available_blocks = blocks.size();
	vector<Pivot> pivots;
	Pivot p;
	p.Point.X = 0;
	p.Point.Y = 0;
	p.Available = true;
	pivots.push_back(p);
	try
	{
		doSolve(pivots, blocks);
	}
	catch (exception e)
	{
		if (strcmp(e.what(), "SOLUTION FOUND") == 0)
		{
			return;
		}
		else
		{
			throw e;
		}
	}
	delete[] board;
}

void doSolve(vector<Pivot> &pivots, vector<Block> &blockLeft) {
	if (CurrentSolutionValue > BestSolutionValue) {
		BestSolution = CurrentSolution;
		BestSolutionValue = CurrentSolutionValue;
	}

	if (available_blocks == 0) {
		throw exception("SOLUTION FOUND");
	}

	for (int i = 0; i < blockLeft.size(); i++)
	{
		if (!blockLeft[i].Available)
			continue;

		available_blocks--;
		blockLeft[i].Available = false;

		for (int p = 0; p < pivots.size(); p++)
		{
			if (!pivots[p].Available)
				continue;
			pivots[p].Available = false;
			processPlacement(pivots, p, blockLeft, i);
			pivots[p].Available = true;
		}

		swap(blockLeft[i].Item.DimensionX, blockLeft[i].Item.DimensionY);
		blockLeft[i].Rotated = true;
		for (int p = 0; p < pivots.size(); p++)
		{
			if (!pivots[p].Available)
				continue;
			pivots[p].Available = false;
			processPlacement(pivots, p, blockLeft, i);
			pivots[p].Available = true;
		}
		swap(blockLeft[i].Item.DimensionX, blockLeft[i].Item.DimensionY);
		blockLeft[i].Rotated = false;

		available_blocks++;
		blockLeft[i].Available = true;
	}
}

void processPlacement(vector<Pivot> &pivots, int p, vector<Block> &blockLeft, int b)
{
	if (intersectsELementsOrBoundary(pivots[p].Point, blockLeft[b].Item))
		return;

	Pivot pp1, pp2;
	pp1.Point.X = pivots[p].Point.X + blockLeft[b].Item.DimensionX;
	pp1.Point.Y = topMostAtX(pivots[p].Point.X + blockLeft[b].Item.DimensionX, pivots[p].Point.Y + blockLeft[b].Item.DimensionY);
	pp1.Available = true;

	pp2.Point.X = leftMostAtY(pivots[p].Point.Y + blockLeft[b].Item.DimensionY, pivots[p].Point.X + blockLeft[b].Item.DimensionX);
	pp2.Point.Y = pivots[p].Point.Y + blockLeft[b].Item.DimensionY;
	pp2.Available = true;
	pivots.push_back(pp1);
	pivots.push_back(pp2);

	pair<Pivot, Block> sol;
	sol.first = pivots[p];
	sol.second = blockLeft[b];
	CurrentSolution.push_back(sol);

	setFlags(pivots[p].Point, blockLeft[b].Item, true);
	CurrentSolutionValue += blockLeft[b].Item.Value;

	doSolve(pivots, blockLeft);

	CurrentSolution.pop_back();
	pivots.pop_back();
	pivots.pop_back();
	CurrentSolutionValue -= blockLeft[b].Item.Value;
	setFlags(pivots[p].Point, blockLeft[b].Item, false);
}

void setFlags(const Point& pivot, const Item& item, bool value)
{
	int maxX = (board_Length <= pivot.X + item.DimensionX) ? board_Length : pivot.X + item.DimensionX;
	int maxY = (board_Height <= pivot.Y + item.DimensionY) ? board_Height : pivot.Y + item.DimensionY;
	for (int i = pivot.X; i < maxX; i++)
	{
		for (int j = pivot.Y; j < maxY; j++)
		{
			board[j * board_Length + i] = value;
		}
	}
}

bool intersectsELementsOrBoundary(const Point &pivot, const Item &item) {
	if (pivot.X < 0 ||
		pivot.Y < 0 ||
		pivot.X + item.DimensionX > board_Length ||
		pivot.Y + item.DimensionY > board_Height)
		return true;

	int maxX = (board_Length <= pivot.X + item.DimensionX) ? board_Length : pivot.X + item.DimensionX;
	int maxY = (board_Height <= pivot.Y + item.DimensionY) ? board_Height : pivot.Y + item.DimensionY;
	for (int j = pivot.Y; j < maxY; j++)
	{
		for (int i = pivot.X; i < maxX; i++)
		{
			if (board[j * board_Length + i])
				return true;
		}
	}

	return false;
}

int topMostAtX(int x, int startAtY) {
	if (startAtY >= board_Height)
		startAtY = board_Height - 1;

	for (int i = startAtY; i >= 0; i--)
	{
		if (board[i * board_Length + x])
			return i + 1;
	}
	return 0;
}

int leftMostAtY(int y, int startAtX) {
	if (startAtX >= board_Length)
		startAtX = board_Length - 1;

	for (int i = startAtX; i >= 0; i--)
	{
		if (board[y * board_Length + i])
			return i + 1;
	}
	return 0;
}

void  printSolution(const vector<pair<Pivot, Block>> &solution) {
	std::cout << "===\n";
	int bestValue = 0;
	for (auto elem : solution)
	{
		bestValue += elem.second.Item.Value;
	}

	cout << "Best solution: " << bestValue << endl << endl;
	cout << "Position | Item Id | Item Length | Item Height | Item Value" << endl;
	for (auto elem : solution)
	{
		cout << elem.first.Point.X << "," << elem.first.Point.Y << " : "
			<< elem.second.Item.Id << ", " << elem.second.Item.DimensionX << ", " << elem.second.Item.DimensionY << ", " << elem.second.Item.Value << endl;
	}
	std::cout << "===\n";
}